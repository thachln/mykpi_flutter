import 'package:flutter/material.dart';

void main() {
  // Refer https://flutter.io
  // https://api.flutter.dev/flutter/material/MaterialApp-class.html

  var app = MaterialApp(home: Scaffold(
    appBar: new AppBar(title: Text('Lets see some images!')),
    body: Text('Hi there!'),
    floatingActionButton: new FloatingActionButton(
      child: Icon(Icons.add),
      onPressed: () {
        print('Hi there!');
      }),

  ));

  runApp(app);
}